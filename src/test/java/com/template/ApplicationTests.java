package com.template;

import com.google.common.collect.ImmutableMap;
import com.template.model.*;
import com.template.service.*;
import com.template.util.SupportedContentType;
import org.apache.commons.compress.utils.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {
    @Autowired
    private UserService userService;
    @Autowired
    private TemplateService templateService;
    @Autowired
    private ImageService imageService;
    @Autowired
    private VariableService variableService;
    @Autowired
    private GenerationService generationService;
    @Autowired
    private TemplateValueService templateValueService;

    @Test
    public void process() throws Exception {
        final File input = new File("src/test/resources/docx/template.docx");
        final User user = userService.existsByUsername("u") ? (User) userService.loadUserByUsername("u") : userService.save(new User().setUsername("u").setPassword("p").setRole(Role.USER));
        final Template template = templateService.save(new Template().setAuthor(user).setName("main").setData(IOUtils.toByteArray(new FileInputStream(input))).setContentType(SupportedContentType.DOCX.getContentType()));
        final Image image = imageService.save(new Image().setName("image.png").setData(IOUtils.toByteArray(new FileInputStream(new File("src/test/resources/docx/image.png")))).setAuthor(user).setContentType(SupportedContentType.PNG.getContentType()));

        final Path out = Paths.get("out/full-result.docx");
        if (!Files.exists(out)) Files.createFile(out);

        Map<String, String> data = ImmutableMap.<String, String>builder()
                .put("header", "The header value")
                .put("title", "Заголовок документа")
                .put("description", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam est felis, tincidunt tristique leo ut, varius vestibulum libero. Fusce et ante mauris. In nisl purus, feugiat ut interdum et, bibendum et urna. Etiam vulputate quam ex, non varius magna laoreet id. Nullam eget dui mattis, feugiat tortor non, fermentum enim. Nam ac odio quis quam condimentum tempor vel interdum lorem. Sed ultricies, eros in consectetur pellentesque, lorem nunc tincidunt ex, eget malesuada ipsum odio nec ex. Cras eu iaculis quam.\n\tQuisque volutpat eros id ipsum consectetur viverra. In blandit mattis lacus, sed malesuada felis maximus sit amet. Aenean vitae massa id tortor scelerisque laoreet. Pellentesque quam ipsum, condimentum posuere faucibus vel, dapibus nec ex. Quisque a orci fringilla, molestie nunc sit amet, ullamcorper leo. Ut posuere volutpat sapien commodo venenatis. Morbi in iaculis eros. Sed a ultrices mauris. Maecenas sollicitudin nulla et viverra tempus. Etiam quis lobortis eros. Fusce eu velit dolor. Maecenas id leo ex. In hac habitasse platea dictumst. Quisque scelerisque odio congue suscipit pretium. In finibus purus sit amet turpis sollicitudin, et bibendum dolor maximus. ")
                .put("image.logo", String.valueOf(image.getId()))
                .put("image.logo2", String.valueOf(image.getId()))
                .put("column1name", "First name")
                .put("column2name", "Last name")
                .put("column3name", "Age")
                .put("row1column1", "John")
                .put("row1column2", "Doe")
                .put("row1column3", "Unknown")
                .put("ullamco", "Strange latin word")
                .put("rehend", "rehenderit Placeholder reprehend")
                .build();

        final Generation generation = generationService.save(new Generation().setName("full-result").setAuthor(user).setTemplate(template));
        data.forEach((k, v) -> {
            final Variable variable = variableService.save(new Variable().setKey(k).setTemplate(template));
            templateValueService.save(new TemplateValue().setId(new TemplateValueId().setGeneration(generation.getId()).setVariable(variable.getId())).setValue(v));
        });

        final Generation result = generationService.process(generation.getId());
        try (FileOutputStream output = new FileOutputStream(out.toFile())) {
            output.write(result.getData());
        }
    }
}