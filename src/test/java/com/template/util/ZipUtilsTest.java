package com.template.util;

import com.google.common.collect.ImmutableMap;
import com.template.model.Image;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.compress.utils.SeekableInMemoryByteChannel;
import org.apache.commons.text.StringSubstitutor;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipOutputStream;

/**
 * @author Taras Zubrei
 */
public class ZipUtilsTest {
    @Test
    public void fromFileToFile() throws IOException {
        File input = new File("src/test/resources/docx/template.docx");
        final Path out = Paths.get("out/result.docx");
        if (!Files.exists(out)) Files.createFile(out);
        final ImmutableMap<String, Image> images = ImmutableMap.of("image.logo", new Image().setName("image.png").setData(IOUtils.toByteArray(new FileInputStream(new File("src/test/resources/docx/image.png")))));
        ZipUtils.parse(
                new ZipFile(new SeekableInMemoryByteChannel(IOUtils.toByteArray(new FileInputStream(input)))),
                new ZipOutputStream(new FileOutputStream(out.toFile())),
                (name, data) -> new StringSubstitutor(ImmutableMap.of(
                        "header", "The header value",
                        "title", "Заголовок документа",
                        "description", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam est felis, tincidunt tristique leo ut, varius vestibulum libero. Fusce et ante mauris. In nisl purus, feugiat ut interdum et, bibendum et urna. Etiam vulputate quam ex, non varius magna laoreet id. Nullam eget dui mattis, feugiat tortor non, fermentum enim. Nam ac odio quis quam condimentum tempor vel interdum lorem. Sed ultricies, eros in consectetur pellentesque, lorem nunc tincidunt ex, eget malesuada ipsum odio nec ex. Cras eu iaculis quam.\n\tQuisque volutpat eros id ipsum consectetur viverra. In blandit mattis lacus, sed malesuada felis maximus sit amet. Aenean vitae massa id tortor scelerisque laoreet. Pellentesque quam ipsum, condimentum posuere faucibus vel, dapibus nec ex. Quisque a orci fringilla, molestie nunc sit amet, ullamcorper leo. Ut posuere volutpat sapien commodo venenatis. Morbi in iaculis eros. Sed a ultrices mauris. Maecenas sollicitudin nulla et viverra tempus. Etiam quis lobortis eros. Fusce eu velit dolor. Maecenas id leo ex. In hac habitasse platea dictumst. Quisque scelerisque odio congue suscipit pretium. In finibus purus sit amet turpis sollicitudin, et bibendum dolor maximus. "
                )).replace(data),
                images::get);
    }
}