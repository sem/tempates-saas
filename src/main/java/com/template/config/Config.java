package com.template.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Optional;

/**
 * @author Taras Zubrei
 */
@Configuration
@EnableJpaAuditing
@EnableScheduling
public class Config {
    @Bean
    public AuditorAware<String> auditorAware() {
        return () -> Optional.of("name");
    }
}
