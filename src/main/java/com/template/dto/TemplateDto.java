package com.template.dto;

import com.template.model.Template;
import com.template.model.User;
import lombok.Data;

/**
 * @author Taras Zubrei
 */
@Data
public class TemplateDto implements Dto<Template> {
    private String name;
    private Long author;

    @Override
    public Template toModel() {
        return new Template()
                .setName(name)
                .setAuthor(new User().setId(author));
    }
}