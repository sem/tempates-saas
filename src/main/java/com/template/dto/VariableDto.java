package com.template.dto;

import com.template.model.Template;
import com.template.model.Variable;
import lombok.Data;

/**
 * @author Taras Zubrei
 */
@Data
public class VariableDto {
    private String key;

    public Variable toModel(Long templateId) {
        return new Variable()
                .setKey(key)
                .setTemplate(new Template().setId(templateId));
    }
}
