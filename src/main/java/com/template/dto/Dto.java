package com.template.dto;

/**
 * @author Taras Zubrei
 */
public interface Dto<T> {
    T toModel();
}