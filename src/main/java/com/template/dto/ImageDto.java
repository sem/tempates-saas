package com.template.dto;

import com.template.model.Image;
import com.template.model.User;
import lombok.Data;

/**
 * @author Taras Zubrei
 */
@Data
public class ImageDto implements Dto<Image> {
    private String name;
    private Long author;

    public Image toModel() {
        return new Image()
                .setName(name)
                .setAuthor(new User().setId(author));
    }
}