package com.template.dto;

import com.template.model.Generation;
import com.template.model.Template;
import com.template.model.User;
import lombok.Data;

/**
 * @author Taras Zubrei
 */
@Data
public class GenerationDto {
    private String name;
    private Long author;

    public Generation toModel(Long templateId) {
        return new Generation()
                .setName(name)
                .setAuthor(new User().setId(author))
                .setTemplate(new Template().setId(templateId));
    }
}