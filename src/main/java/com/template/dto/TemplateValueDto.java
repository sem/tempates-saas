package com.template.dto;

import com.template.model.TemplateValue;
import com.template.model.TemplateValueId;
import lombok.Data;

/**
 * @author Taras Zubrei
 */
@Data
public class TemplateValueDto {
    private Long variable;
    private String value;

    public TemplateValue toModel(Long generationId) {
        return new TemplateValue()
                .setId(new TemplateValueId().setVariable(variable).setGeneration(generationId))
                .setValue(value);
    }
}