package com.template.dto;

import com.template.model.User;
import lombok.Data;

/**
 * @author Taras Zubrei
 */
@Data
public class UserDto implements Dto<User> {
    private Long id;
    private String username;
    private String password;

    @Override
    public User toModel() {
        return new User()
                .setId(id)
                .setUsername(username)
                .setPassword(password);
    }
}
