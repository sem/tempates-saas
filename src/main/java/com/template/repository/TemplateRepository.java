package com.template.repository;

import com.template.model.Template;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface TemplateRepository extends JpaRepository<Template, Long> {
    List<Template> findByAuthor_Id(Long id);

    List<Template> findByAuthor_IdAndName(Long id, String name);

    Page<Template> findAllByAuthor_Id(Pageable pageable, Long authorId);
}
