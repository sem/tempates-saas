package com.template.repository;

import com.template.model.Variable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface VariableRepository extends JpaRepository<Variable, Long> {
    List<Variable> findByTemplate_Id(Long templateId);

    @Query("select v from Variable as v inner join Generation as g on g.id=?1 where v.template=g.template")
    List<Variable> findByGeneration(Long generationId);

    @Query("select u.id from Variable as v inner join Template as t on t.id=v.template inner join User as u on u.id=t.author where v.id=?1")
    Long findAuthor(Long id);
}