package com.template.repository;

import com.template.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface ImageRepository extends JpaRepository<Image, Long> {
    List<Image> findByAuthor_Id(Long id);

    List<Image> findByAuthor_IdAndName(Long id, String name);

    void deleteAllByCreatedAtBefore(LocalDateTime date);
}
