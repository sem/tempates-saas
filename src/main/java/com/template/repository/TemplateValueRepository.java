package com.template.repository;

import com.template.model.TemplateValue;
import com.template.model.TemplateValueId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface TemplateValueRepository extends JpaRepository<TemplateValue, TemplateValueId> {
    List<TemplateValue> findAllById_Generation(Long generationId);
}