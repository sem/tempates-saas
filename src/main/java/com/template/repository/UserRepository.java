package com.template.repository;

import com.template.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Taras Zubrei
 */
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("from User as u left join fetch u.templates where u.id=?1")
    User findFullById(Long id);

    User findByUsername(String username);
}
