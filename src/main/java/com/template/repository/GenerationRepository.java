package com.template.repository;

import com.template.model.Generation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface GenerationRepository extends JpaRepository<Generation, Long> {
    @Query("from Generation as g left join fetch g.template where g.id=?1 ")
    Generation findFullById(Long id);

    @Query("select g.author.id from Generation as g where g.id=?1")
    Long findAuthor(Long id);

    List<Generation> findAllByTemplate_Id(Long templateId);
}