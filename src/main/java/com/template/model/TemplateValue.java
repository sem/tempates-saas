package com.template.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @author Taras Zubrei
 */
@Data
@Entity
@Table(name = "template_values")
@Accessors(chain = true)
public class TemplateValue {
    @EmbeddedId
    private TemplateValueId id;
    private String value;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "variable_id", nullable = false, insertable = false, updatable = false)
    private Variable variable;
}