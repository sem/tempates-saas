package com.template.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY;

/**
 * @author Taras Zubrei
 */
@Data
@Entity
@Table(name = "templates")
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Template implements BaseEntityWithAttachment<Template> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "templates_id_generator")
    @SequenceGenerator(name = "templates_id_generator", sequenceName = "templates_id_seq", allocationSize = 1)
    private Long id;
    private String name;
    @CreatedDate
    @JsonProperty(access = READ_ONLY)
    private LocalDateTime createdAt;
    @LastModifiedDate
    @JsonProperty(access = READ_ONLY)
    private LocalDateTime lastUpdatedAt;
    @JsonIgnore
    private byte[] data;
    @JsonProperty(access = READ_ONLY)
    private String contentType;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author", nullable = false)
    @ToString.Exclude
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private User author;
}
