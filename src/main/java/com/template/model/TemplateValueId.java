package com.template.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Taras Zubrei
 */
@Data
@Embeddable
@Accessors(chain = true)
public class TemplateValueId implements Serializable {
    @Column(name = "generation_id")
    private Long generation;
    @Column(name = "variable_id")
    private Long variable;
}