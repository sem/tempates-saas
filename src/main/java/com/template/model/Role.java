package com.template.model;

/**
 * @author Taras Zubrei
 */
public enum Role {
    USER, ADMIN
}