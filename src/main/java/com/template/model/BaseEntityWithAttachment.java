package com.template.model;

/**
 * @author Taras Zubrei
 */
public interface BaseEntityWithAttachment<T extends BaseEntityWithAttachment> {
    byte[] getData();

    T setData(byte[] data);

    String getContentType();

    T setContentType(String contentType);

    User getAuthor();

    T setAuthor(User author);
}
