package com.template.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY;

/**
 * @author Taras Zubrei
 */
@Data
@Entity
@Table(name = "variables")
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Variable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "variables_id_generator")
    @SequenceGenerator(name = "variables_id_generator", sequenceName = "variables_id_seq", allocationSize = 1)
    private Long id;
    private String
            key;
    @CreatedDate
    @JsonProperty(access = READ_ONLY)
    private LocalDateTime createdAt;
    @LastModifiedDate
    @JsonProperty(access = READ_ONLY)
    private LocalDateTime lastUpdatedAt;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "template", nullable = false)
    @ToString.Exclude
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Template template;
}