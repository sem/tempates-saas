package com.template.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.dto.TemplateDto;
import com.template.exception.InvalidContentTypeException;
import com.template.model.Template;
import com.template.service.Service;
import com.template.service.TemplateService;
import com.template.util.SupportedContentType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;
import java.util.Optional;

import static com.template.web.BaseController.getCurrentUser;

/**
 * @author Taras Zubrei
 */
@RestController
@RequestMapping("api/template")
@RequiredArgsConstructor
public class TemplateController extends BaseControllerWithAttachment<Template, Long, TemplateDto> {
    private final TemplateService service;
    private final ObjectMapper mapper;

    @Override
    protected void validate(Template value, MultipartFile file) {
        if (SupportedContentType.TEMPLATES.stream()
                .map(SupportedContentType::getContentType)
                .noneMatch(Objects.requireNonNull(file.getContentType())::equals)) {
            throw new InvalidContentTypeException(file.getContentType(), SupportedContentType.TEMPLATES);
        }
    }

    @GetMapping
    public Page<Template> findAll(@RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "size", required = false) Integer size) {
        return service.findByAuthor(
                PageRequest.of(Optional.ofNullable(page).orElse(0), Optional.ofNullable(size).orElse(20)),
                getCurrentUser().getId()
        );
    }

    @Override
    protected Class<TemplateDto> dtoClass() {
        return TemplateDto.class;
    }

    @Override
    protected Service<Template, Long> service() {
        return service;
    }

    @Override
    protected ObjectMapper mapper() {
        return mapper;
    }
}