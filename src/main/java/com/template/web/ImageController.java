package com.template.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.dto.ImageDto;
import com.template.exception.InvalidContentTypeException;
import com.template.model.Image;
import com.template.service.ImageService;
import com.template.service.Service;
import com.template.util.SupportedContentType;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

/**
 * @author Taras Zubrei
 */
@RestController
@RequestMapping("api/image")
@RequiredArgsConstructor
public class ImageController extends BaseControllerWithAttachment<Image, Long, ImageDto> {
    private final ImageService service;
    private final ObjectMapper mapper;

    @Override
    protected void validate(Image value, MultipartFile file) {
        if (SupportedContentType.IMAGES.stream()
                .map(SupportedContentType::getContentType)
                .noneMatch(Objects.requireNonNull(file.getContentType())::equals)) {
            throw new InvalidContentTypeException(file.getContentType(), SupportedContentType.IMAGES);
        }
    }

    @Override
    protected Class<ImageDto> dtoClass() {
        return ImageDto.class;
    }

    @Override
    protected Service<Image, Long> service() {
        return service;
    }

    @Override
    protected ObjectMapper mapper() {
        return mapper;
    }
}