package com.template.web;

import com.template.dto.VariableDto;
import com.template.model.Variable;
import com.template.service.TemplateService;
import com.template.service.VariableService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.template.web.BaseController.validateUserAccess;

/**
 * @author Taras Zubrei
 */
@RestController
@RequestMapping("api/template/{templateId}/variable")
@RequiredArgsConstructor
public class VariableController {
    private final VariableService service;
    private final TemplateService templateService;

    @GetMapping
    public List<Variable> findAll(@PathVariable Long templateId) {
        validateUserAccess(templateService.findOne(templateId).getAuthor().getId());
        return service.findByTemplate(templateId);
    }

    @GetMapping("/{id}")
    public Variable findOne(@PathVariable Long id) {
        validate(id, null);
        return service.findOne(id);
    }

    @PostMapping
    public Variable save(@RequestBody VariableDto body, @PathVariable Long templateId) {
        final Variable variable = body.toModel(templateId);
        validate(null, variable);
        return service.save(variable);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        validate(id, null);
        service.delete(id);
    }

    @PostMapping("/batch")
    public List<Variable> save(@RequestBody List<VariableDto> body, @PathVariable Long templateId) {
        validateUserAccess(templateService.findOne(templateId).getAuthor().getId());
        return service.save(
                body.stream()
                        .map(variable -> variable.toModel(templateId))
                        .collect(Collectors.toList())
        );
    }

    private void validate(Long id, Variable body) {
        if (id != null)
            validateUserAccess(service.findAuthor(id));
        if (body != null)
            validateUserAccess(templateService.findOne(body.getTemplate().getId()).getAuthor().getId());
        if (id == null && body == null)
            validateUserAccess(null);
    }
}