package com.template.web;

import com.template.exception.NoResourceException;
import com.template.exception.ResourceAccessForbidden;
import com.template.model.BaseEntityWithAttachment;
import com.template.model.Role;
import com.template.service.Service;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.template.web.BaseController.getCurrentUser;
import static com.template.web.BaseController.validateUserAccess;

/**
 * @author Taras Zubrei
 */
public abstract class BaseControllerWithReadonlyAttachment<T extends BaseEntityWithAttachment, K> {
    protected abstract Service<T, K> service();

    @GetMapping("/{id}")
    public T findOne(@PathVariable K id) {
        validate(id);
        return service().findOne(id);
    }

    @GetMapping("/{id}/file")
    public ResponseEntity<byte[]> getFile(@PathVariable K id) {
        validate(id);
        final T t = service().findOne(id);
        final byte[] data = t.getData();
        if (data == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok()
                .contentLength(data.length)
                .contentType(MediaType.parseMediaType(t.getContentType()))
                .body(data);
    }

    protected void validate(T value, MultipartFile file) {
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable K id) {
        validate(id);
        service().delete(id);
    }

    protected T populate(T body, MultipartFile file) throws IOException {
        T t = file == null ? body : (T) body.setContentType(file.getContentType()).setData(file.getBytes());
        if (getCurrentUser().getRole() == Role.ADMIN)
            return t;
        else
            return (T) t.setAuthor(getCurrentUser());
    }

    protected void validate(K id) {
        try {
            validateUserAccess(service().findOne(id).getAuthor().getId());
        } catch (NoResourceException ex) {
            throw new ResourceAccessForbidden();
        }
    }
}