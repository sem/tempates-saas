package com.template.web;

import com.template.dto.Dto;
import com.template.exception.ResourceAccessForbidden;
import com.template.model.Role;
import com.template.model.User;
import com.template.service.Service;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import java.util.Objects;

/**
 * @author Taras Zubrei
 */
public abstract class BaseController<T, K, D extends Dto<T>> {
    @GetMapping("/{id}")
    public T findOne(@PathVariable K id) {
        validate(id, null);
        return service().findOne(id);
    }

    @PostMapping
    public T save(@RequestBody D body) {
        final T model = body.toModel();
        validate(null, model);
        return service().save(model);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable K id) {
        validate(id, null);
        service().delete(id);
    }

    protected abstract Service<T, K> service();

    abstract void validate(K id, T body);

    static void validateUserAccess(Long userId) {
        final User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
            if (!Objects.equals(userId, principal.getId()) && principal.getRole() != Role.ADMIN)
                throw new ResourceAccessForbidden();
        } catch (NoResultException ex) {
            throw new ResourceAccessForbidden();
        }
    }

    static User getCurrentUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
