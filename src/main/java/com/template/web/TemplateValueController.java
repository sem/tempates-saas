package com.template.web;

import com.template.dto.TemplateValueDto;
import com.template.model.TemplateValue;
import com.template.service.GenerationService;
import com.template.service.TemplateValueService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.template.web.BaseController.validateUserAccess;

/**
 * @author Taras Zubrei
 */
@RestController
@RequestMapping("api/generation/{generationId}/value")
@RequiredArgsConstructor
public class TemplateValueController {
    private final TemplateValueService service;
    private final GenerationService generationService;

    @GetMapping
    public List<TemplateValue> findAll(@PathVariable Long generationId) {
        validateUserAccess(generationService.findAuthor(generationId));
        return service.findByGeneration(generationId);
    }

    @PostMapping
    public TemplateValue save(@RequestBody TemplateValueDto body, @PathVariable Long generationId) {
        validateUserAccess(generationService.findAuthor(generationId));
        return service.save(body.toModel(generationId));
    }

    @PostMapping("/batch")
    public List<TemplateValue> save(@RequestBody List<TemplateValueDto> body, @PathVariable Long generationId) {
        validateUserAccess(generationService.findAuthor(generationId));
        return service.save(
                body.stream()
                        .map(value -> value.toModel(generationId))
                        .collect(Collectors.toList())
        );
    }
}