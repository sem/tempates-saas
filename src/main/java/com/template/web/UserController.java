package com.template.web;

import com.template.dto.UserDto;
import com.template.model.User;
import com.template.service.Service;
import com.template.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Taras Zubrei
 */
@RestController
@RequestMapping("api/user")
@RequiredArgsConstructor
public class UserController extends BaseController<User, Long, UserDto> {
    private final UserService service;
    private final PasswordEncoder passwordEncoder;

    @Override
    protected Service<User, Long> service() {
        return service;
    }

    @Override
    protected void validate(Long id, User user) {
        if (id != null)
            validateUserAccess(id);
        if (user != null) {
            if (user.getId() == null)
                user.setId(getCurrentUser().getId());
            else
                validateUserAccess(user.getId());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        if (id == null && user == null)
            validateUserAccess(null);
    }
}