package com.template.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.dto.Dto;
import com.template.model.BaseEntityWithAttachment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author Taras Zubrei
 */
public abstract class BaseControllerWithAttachment<T extends BaseEntityWithAttachment, K, D extends Dto<T>> extends BaseControllerWithReadonlyAttachment<T, K> {
    protected abstract ObjectMapper mapper();

    protected abstract Class<D> dtoClass();

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public T save(@RequestBody D body) throws IOException {
        return service().save(populate(body.toModel(), null));
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public T save(@RequestParam String body, @RequestParam MultipartFile file) throws IOException {
        final D value = mapper().readValue(body, dtoClass());
        final T model = value.toModel();
        validate(model, file);
        return service().save(populate(model, file));
    }

    @PutMapping("/{id}")
    public T upload(@PathVariable K id, @RequestParam("file") MultipartFile file) throws IOException {
        final T t = service().findOne(id);
        validate(t, file);
        return service().save(populate(t, file));
    }
}
