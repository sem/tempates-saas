package com.template.web;

import com.template.dto.GenerationDto;
import com.template.exception.NoResourceException;
import com.template.exception.ResourceAccessForbidden;
import com.template.model.Generation;
import com.template.model.Role;
import com.template.model.User;
import com.template.service.GenerationService;
import com.template.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

import static com.template.web.BaseController.getCurrentUser;
import static com.template.web.BaseController.validateUserAccess;

/**
 * @author Taras Zubrei
 */
@RestController
@RequestMapping("api/template/{templateId}/generation")
@RequiredArgsConstructor
public class GenerationController extends BaseControllerWithReadonlyAttachment<Generation, Long> {
    private final GenerationService service;
    private final TemplateService templateService;

    @GetMapping
    public List<Generation> findAll(@PathVariable Long templateId) {
        return service.findByTemplate(templateId);
    }

    @PostMapping
    public Generation save(@RequestBody GenerationDto body, @PathVariable Long templateId) {
        if (getCurrentUser().getRole() != Role.ADMIN)
            body.setAuthor(getCurrentUser().getId());
        final Generation generation = body.toModel(templateId);
        validate(generation.getAuthor().getId(), templateId);
        return service.save(generation);
    }

    @PostMapping("/{id}/process")
    public ResponseEntity<byte[]> process(@PathVariable Long id) {
        validate(id);
        final Generation generation = service.process(id);
        final byte[] data = generation.getData();
        return ResponseEntity.ok()
                .contentLength(data.length)
                .contentType(MediaType.parseMediaType(generation.getContentType()))
                .body(data);
    }

    @Override
    protected GenerationService service() {
        return service;
    }

    private void validate(Long userId, Long templateId) {
        try {
            final User author = templateService.findOne(templateId).getAuthor();
            validateUserAccess(author.getId());
            if (!Objects.equals(author.getId(), userId)) throw new ResourceAccessForbidden();
        } catch (NoResourceException ex) {
            throw new ResourceAccessForbidden();
        }
    }
}