package com.template.web;

import com.template.exception.DuplicateUsernameException;
import com.template.exception.NoTokenProvidedException;
import com.template.model.Role;
import com.template.model.User;
import com.template.security.JwtTokenProvider;
import com.template.security.LoginRequest;
import com.template.security.SignUpRequest;
import com.template.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Optional;

/**
 * @author Taras Zubrei
 */
@RestController
@RequestMapping("api/auth/")
@RequiredArgsConstructor
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider tokenProvider;

    @PostMapping("signin")
    public ResponseEntity authenticateUser(@RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok().header("Authorization", "Bearer " + login(
                loginRequest.getUsername(),
                loginRequest.getPassword()
        )).build();
    }

    @GetMapping("refresh")
    public ResponseEntity refreshToken(HttpServletRequest request) {
        return ResponseEntity.ok().header("Authorization", "Bearer " + tokenProvider.refreshToken(
                Optional.ofNullable(tokenProvider.getJwtFromRequest(request)).orElseThrow(NoTokenProvidedException::new)
        )).build();
    }

    @PostMapping("signup")
    public ResponseEntity<?> registerUser(@RequestBody SignUpRequest signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())) throw new DuplicateUsernameException();
        User user = new User()
                .setUsername(signUpRequest.getUsername())
                .setPassword(passwordEncoder.encode(signUpRequest.getPassword()))
                .setRole(Role.USER);

        User result = userService.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{id}")
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).header("Authorization", "Bearer " + login(
                signUpRequest.getUsername(),
                signUpRequest.getPassword()
        )).body(result);
    }

    private String login(String username, String password) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return tokenProvider.generateToken(authentication);
    }
}