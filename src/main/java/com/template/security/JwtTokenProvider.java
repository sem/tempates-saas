package com.template.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.exception.TokenExpiredLongAgoException;
import com.template.model.User;
import com.template.util.DateUtils;
import com.template.util.ExceptionUtils;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Taras Zubrei
 */
@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);
    private final ObjectMapper objectMapper;

    @Value("${app.jwtSecret}")
    private String jwtSecret;
    @Value("${app.expirationDays}")
    private int expirationDays;
    @Value("${app.refreshExpirationDays}")
    private int refreshExpirationDays;

    public String generateToken(Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(Long.toString(user.getId()))
                .setIssuedAt(new Date())
                .setExpiration(Date.from(LocalDateTime.now().plusDays(expirationDays).atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
        return Long.parseLong(claims.getSubject());
    }

    public String refreshToken(String token) {
        LocalDateTime expiration = LocalDateTime.now().plusDays(expirationDays);
        Map<String, Object> claims = ExceptionUtils.wrap(() -> objectMapper.reader().forType(HashMap.class).readValue(new String(TextCodec.BASE64URL.decode(token.split("\\.")[1]))));
        if (claims.get("exp") != null && claims.get("exp") instanceof Integer) {
            LocalDateTime previousExpiration = DateUtils.toLocalDateTime(new Date(((Integer) claims.get("exp")).longValue() * 1000));
            if (Duration.between(previousExpiration, expiration).abs().toDays() <= refreshExpirationDays)
                return Jwts.builder()
                        .setClaims(claims)
                        .setExpiration(DateUtils.toDate(expiration))
                        .signWith(SignatureAlgorithm.HS256, jwtSecret)
                        .compact();
        }
        throw new TokenExpiredLongAgoException();
    }

    public String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }
}