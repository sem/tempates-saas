package com.template.security;

import lombok.Data;

/**
 * @author Taras Zubrei
 */
@Data
public class SignUpRequest {
    private String username;
    private String password;
}