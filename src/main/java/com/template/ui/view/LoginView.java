package com.template.ui.view;

import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.*;

import java.util.Collections;

@Route(value = "login")
@PageTitle(value = "Login")
@Viewport("width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes, viewport-fit=cover")
public class LoginView extends VerticalLayout implements AfterNavigationObserver, BeforeEnterObserver {

    private LoginOverlay login = new LoginOverlay();

//	public LoginView() {
//		LoginI18n i18n = LoginI18n.createDefault();
//		i18n.setHeader(new LoginI18n.Header());
//		i18n.getHeader().setTitle("My Starter Project");
//		i18n.getHeader().setDescription(
//			"admin@vaadin.com + admin\n" + "barista@vaadin.com + barista");
//		i18n.setAdditionalInformation(null);
//		i18n.setForm(new LoginI18n.Form());
//		i18n.getForm().setSubmit("Sign in");
//		i18n.getForm().setTitle("Sign in");
//		i18n.getForm().setUsername("Email");
//		i18n.getForm().setPassword("Password");
//		setI18n(i18n);
//		setForgotPasswordButtonVisible(false);
//		setAction("login");
//	}

    public LoginView() {
        login.setAction("login");
        login.setOpened(true);
        login.setTitle("Login Title");
        login.setDescription("Login");
        login.setForgotPasswordButtonVisible(false);
        getElement().appendChild(login.getElement());
        setClassName("login-view");





//        LoginI18n i18n = LoginI18n.createDefault();
//		i18n.setHeader(new LoginI18n.Header());
//		i18n.getHeader().setTitle("My Starter Project");
//		i18n.getHeader().setDescription(
//			"admin@vaadin.com + admin\n" + "barista@vaadin.com + barista");
//		i18n.setAdditionalInformation(null);
//		i18n.setForm(new LoginI18n.Form());
//		i18n.getForm().setSubmit("Sign in");
//		i18n.getForm().setTitle("Sign in");
//		i18n.getForm().setUsername("Email");
//		i18n.getForm().setPassword("Password");
//		login.setI18n(i18n);
//        login.setForgotPasswordButtonVisible(false);
//		login.setAction("login");
    }


    //	@Override
//	public void beforeEnter(BeforeEnterEvent event) {
////		if (SecurityUtils.isUserLoggedIn()) {
////			event.forwardTo(StorefrontView.class);
////		} else {
////			setOpened(true);
////		}
//	}
//


    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if(!event.getLocation().getQueryParameters().getParameters().getOrDefault("error", Collections.emptyList()).isEmpty()) {
            login.setError(true);
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
//        setError(event.getLocation().getQueryParameters().getParameters().containsKey("error"));
    }
}