package com.template.exception;

import com.template.util.SupportedContentType;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Taras Zubrei
 */
@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
public class InvalidContentTypeException extends RuntimeException {
    public InvalidContentTypeException(String contentType, List<SupportedContentType> supportedContentTypes) {
        super("Invalid content type: " + contentType + ". Supported types are: " + supportedContentTypes.stream().map(SupportedContentType::getContentType).collect(Collectors.joining(", ")));
    }
}
