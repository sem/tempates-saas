package com.template.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Taras Zubrei
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoResourceException extends RuntimeException {
    public NoResourceException(Object id) {
        super("No resource found for given id: " + id);
    }
}