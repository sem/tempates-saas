package com.template.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Taras Zubrei
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class TokenExpiredLongAgoException extends RuntimeException {
    public TokenExpiredLongAgoException() {
        super("Cannot refresh token which expired too long ago. Please login and create new one");
    }
}