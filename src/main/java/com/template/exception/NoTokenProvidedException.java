package com.template.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Taras Zubrei
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class NoTokenProvidedException extends RuntimeException {
    public NoTokenProvidedException() {
        super("Token required for this request. Provide via header in following format 'Authorization: Bearer {token}'");
    }
}