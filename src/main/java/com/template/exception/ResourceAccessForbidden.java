package com.template.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Taras Zubrei
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class ResourceAccessForbidden extends RuntimeException {
    public ResourceAccessForbidden() {
        super("You have no access to this resource");
    }
}