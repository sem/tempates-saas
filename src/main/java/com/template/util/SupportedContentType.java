package com.template.util;

import java.util.Arrays;
import java.util.List;

/**
 * @author Taras Zubrei
 */
public enum SupportedContentType {
    DOCX("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
    PNG("image/png"),
    JPEG("image/jpeg"),
    GIF("image/gif");

    public static final List<SupportedContentType> IMAGES = Arrays.asList(
            PNG, JPEG, GIF
    );
    public static final List<SupportedContentType> TEMPLATES = Arrays.asList(
            DOCX
    );
    private final String contentType;

    SupportedContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }
}
