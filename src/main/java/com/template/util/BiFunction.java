package com.template.util;

/**
 * @author Taras Zubrei
 */
@FunctionalInterface
public interface BiFunction<T, U, R> {
    R apply(T t, U u) throws Throwable;
}
