package com.template.util;

import java.io.IOException;
import java.io.UncheckedIOException;

/**
 * @author Taras Zubrei
 */
public class ExceptionUtils {
    public static <T> T wrap(Supplier<T> supplier) {
        try {
            return supplier.get();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @FunctionalInterface
    public interface Supplier<T> {
        T get() throws IOException;
    }
}