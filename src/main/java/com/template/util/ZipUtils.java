package com.template.util;

import com.google.common.collect.ImmutableMap;
import com.template.model.Image;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Taras Zubrei
 */
public class ZipUtils {
    private static final Logger logger = LoggerFactory.getLogger(ZipUtils.class);
    private static final Map<FileType, String> SKIP_FILES = ImmutableMap.of(FileType.DOCUMENT_RELS, "word/_rels/document.xml.rels");

    public static void writeEntry(ZipOutputStream zos, String name, byte[] data) {
        try {
            ZipEntry result = new ZipEntry(name);
            zos.putNextEntry(result);
            zos.write(data);
            zos.closeEntry();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    public static void writeEntry(ZipOutputStream zos, String name, ByteArrayOutputStream data) {
        try {
            ZipEntry result = new ZipEntry(name);
            zos.putNextEntry(result);
            data.writeTo(zos);
            zos.closeEntry();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    public static void parse(ZipFile input, ZipOutputStream output, BiFunction<String, String, String> transformer, Function<String, Image> imageReplacer) {
        try (ZipOutputStream zos = output) {
            final Map<String, String> images = new HashMap<>();
            for (ZipArchiveEntry entry : Collections.list(input.getEntries())) {
                if (SKIP_FILES.values().contains(entry.getName())) continue;
                if (entry.getName().endsWith("xml")) {
                    logger.debug("Working with: " + entry.getName());
                    final String data = new String(IOUtils.toByteArray(input.getInputStream(entry)));
                    images.putAll(searchForImages(data));
                    final String response = transformer.apply(entry.getName(), removeImageExpressions(data));
                    writeEntry(zos, entry.getName(), response.getBytes());
                } else {
                    writeEntry(zos, entry.getName(), IOUtils.toByteArray(input.getInputStream(entry)));
                }
            }
            for (Map.Entry<FileType, String> entry : SKIP_FILES.entrySet()) {
                final ZipArchiveEntry zipEntry = input.getEntry(entry.getValue());
                if (zipEntry == null) continue;
                final String data = new String(IOUtils.toByteArray(input.getInputStream(zipEntry)));
                String response = data;
                if (entry.getKey() == FileType.DOCUMENT_RELS) {
                    response = replaceImages(zos, data, images, imageReplacer);
                }
                writeEntry(zos, zipEntry.getName(), response.getBytes());
            }
            zos.finish();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        } catch (Throwable t) {
            throw new IllegalStateException(t);
        }
    }

    private static Map<String, String> searchForImages(String payload) {
        final Document document = Jsoup.parse(payload);
        return document.getElementsByTag("pic:cNvPr").stream()
                .filter(el -> el.attr("name") != null && el.attr("name").contains("${image."))
                .collect(Collectors.toMap(el -> el.parent().parent().getElementsByAttribute("r:embed").get(0).attr("r:embed"), el -> el.attr("name").substring(2, el.attr("name").length() - 1)));
    }

    private static String removeImageExpressions(String payload) {
        return payload.replaceAll("\\$\\{image\\.([\\w\\d.]+)}", "image.$1");
    }

    private static String replaceImages(ZipOutputStream output, String payload, Map<String, String> images, Function<String, Image> replacer) {
        final Document document = Jsoup.parse(payload, "", Parser.xmlParser());
        images.forEach((id, placeholder) -> {
            final Image image = replacer.apply(placeholder);
            final String path = "media/" + UUID.randomUUID() + "-" + image.getName();
            writeEntry(output, "word/" + path, image.getData());
            document.getElementsByAttributeValue("Id", id).attr("Target", path);
        });
        return document.outerHtml();
    }
}
