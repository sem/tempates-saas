package com.template.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Taras Zubrei
 */
public class DateUtils {
    public static LocalDateTime toLocalDateTime(Date date) {
        return checkNotNull(date).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date toDate(LocalDateTime localDate) {
        return Date.from(checkNotNull(localDate).atZone(ZoneId.systemDefault()).toInstant());
    }
}