package com.template.service;

import com.template.model.Template;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface TemplateService extends Service<Template, Long> {
    List<Template> find(Long userId);

    List<Template> search(Long userId, String name);

    Page<Template> findByAuthor(Pageable pageable, Long userId);
}
