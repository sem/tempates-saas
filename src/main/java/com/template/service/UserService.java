package com.template.service;

import com.template.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Taras Zubrei
 */
public interface UserService extends Service<User, Long>, UserDetailsService {
    boolean existsByUsername(String username);
}
