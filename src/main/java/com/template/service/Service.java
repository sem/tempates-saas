package com.template.service;

/**
 * @author Taras Zubrei
 */
public interface Service<T, K> {
    T findOne(K id);

    T save(T object);

    void delete(K id);
}
