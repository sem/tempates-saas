package com.template.service;

import com.template.model.Image;

import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface ImageService extends Service<Image, Long> {
    List<Image> findByUser(Long userId);

    List<Image> search(Long userId, String name);
}
