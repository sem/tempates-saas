package com.template.service;

import com.template.model.TemplateValue;
import com.template.model.TemplateValueId;

import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface TemplateValueService extends Service<TemplateValue, TemplateValueId> {
    List<TemplateValue> findByGeneration(Long generationId);

    List<TemplateValue> save(List<TemplateValue> values);
}