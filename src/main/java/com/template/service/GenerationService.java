package com.template.service;

import com.template.model.Generation;

import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface GenerationService extends Service<Generation, Long> {
    Generation findFullById(Long id);

    Generation process(Long id);

    Long findAuthor(Long id);

    List<Generation> findByTemplate(Long templateId);
}