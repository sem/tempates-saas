package com.template.service.impl;

import com.template.model.Image;
import com.template.repository.ImageRepository;
import com.template.service.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Taras Zubrei
 */
@Service
@RequiredArgsConstructor
public class ImageServiceImpl extends BaseService<Image, Long> implements ImageService {
    private final ImageRepository repository;

    @Scheduled(cron = "0 0 0 * * ?")
    public void cleanDB() {
        repository.deleteAllByCreatedAtBefore(LocalDateTime.now().minusHours(1));
    }

    @Override
    public List<Image> findByUser(Long userId) {
        return repository.findByAuthor_Id(userId);
    }

    @Override
    public List<Image> search(Long userId, String name) {
        return repository.findByAuthor_IdAndName(userId, name);
    }

    @Override
    protected JpaRepository<Image, Long> repository() {
        return repository;
    }
}
