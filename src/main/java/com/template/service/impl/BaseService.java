package com.template.service.impl;

import com.template.exception.NoResourceException;
import com.template.service.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Taras Zubrei
 */
@Transactional
public abstract class BaseService<T, K> implements Service<T, K> {

    @Override
    public T findOne(K id) {
        return repository().findById(id).orElseThrow(() -> new NoResourceException(id));
    }

    @Override
    public T save(T object) {
        return repository().save(object);
    }

    @Override
    public void delete(K id) {
        repository().deleteById(id);
    }

    protected abstract JpaRepository<T, K> repository();
}
