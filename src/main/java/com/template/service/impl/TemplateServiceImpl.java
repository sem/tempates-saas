package com.template.service.impl;

import com.template.model.Template;
import com.template.repository.TemplateRepository;
import com.template.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Taras Zubrei
 */
@Service
@RequiredArgsConstructor
public class TemplateServiceImpl extends BaseService<Template, Long> implements TemplateService {
    private final TemplateRepository repository;

    @Override
    public List<Template> find(Long userId) {
        return repository.findByAuthor_Id(userId);
    }

    @Override
    public List<Template> search(Long userId, String name) {
        return repository.findByAuthor_IdAndName(userId, name);
    }

    @Override
    public Page<Template> findByAuthor(Pageable pageable, Long userId) {
        return repository.findAllByAuthor_Id(pageable, userId);
    }

    @Override
    protected JpaRepository<Template, Long> repository() {
        return repository;
    }
}
