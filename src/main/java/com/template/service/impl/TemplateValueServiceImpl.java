package com.template.service.impl;

import com.template.model.TemplateValue;
import com.template.model.TemplateValueId;
import com.template.repository.TemplateValueRepository;
import com.template.service.TemplateValueService;
import com.template.service.VariableService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Taras Zubrei
 */
@Service
@RequiredArgsConstructor
public class TemplateValueServiceImpl extends BaseService<TemplateValue, TemplateValueId> implements TemplateValueService {
    private final TemplateValueRepository repository;
    private final VariableService variableService;

    @Override
    public List<TemplateValue> findByGeneration(Long generationId) {
        final Map<Long, TemplateValue> values = repository.findAllById_Generation(generationId).stream()
                .collect(Collectors.toMap(value -> value.getId().getVariable(), Function.identity()));
        return variableService.findByGeneration(generationId)
                .stream()
                .map(variable -> values.getOrDefault(variable.getId(),
                        new TemplateValue().setId(
                                new TemplateValueId().setGeneration(generationId).setVariable(variable.getId())
                        ).setVariable(variable)))
                .collect(Collectors.toList());
    }

    @Override
    public List<TemplateValue> save(List<TemplateValue> values) {
        return repository.saveAll(values);
    }

    @Override
    protected TemplateValueRepository repository() {
        return repository;
    }
}