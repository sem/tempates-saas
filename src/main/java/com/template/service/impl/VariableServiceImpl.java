package com.template.service.impl;

import com.template.model.Variable;
import com.template.repository.VariableRepository;
import com.template.service.VariableService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Taras Zubrei
 */
@Service
@RequiredArgsConstructor
public class VariableServiceImpl extends BaseService<Variable, Long> implements VariableService {
    private final VariableRepository repository;

    @Override
    public List<Variable> findByTemplate(Long templateId) {
        return repository.findByTemplate_Id(templateId);
    }

    @Override
    public List<Variable> findByGeneration(Long generationId) {
        return repository.findByGeneration(generationId);
    }

    @Override
    public Long findAuthor(Long id) {
        return repository.findAuthor(id);
    }

    @Override
    public List<Variable> save(List<Variable> values) {
        return repository.saveAll(values);
    }

    @Override
    protected VariableRepository repository() {
        return repository;
    }
}