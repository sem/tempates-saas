package com.template.service.impl;

import com.template.model.Generation;
import com.template.model.TemplateValue;
import com.template.repository.GenerationRepository;
import com.template.service.GenerationService;
import com.template.service.ImageService;
import com.template.service.TemplateValueService;
import com.template.util.ZipUtils;
import freemarker.template.Configuration;
import lombok.RequiredArgsConstructor;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.SeekableInMemoryByteChannel;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

/**
 * @author Taras Zubrei
 */
@Service
@RequiredArgsConstructor
public class GenerationServiceImpl extends BaseService<Generation, Long> implements GenerationService {
    private final GenerationRepository repository;
    private final TemplateValueService templateValueService;
    private final ImageService imageService;
    private final Configuration freemarkerConfig;

    @Override
    protected GenerationRepository repository() {
        return repository;
    }

    @Override
    public Generation findFullById(Long id) {
        return repository.findFullById(id);
    }

    @Override
    public Generation process(Long id) {
        final Generation generation = findFullById(id);
        final Map<String, String> model = templateValueService.findByGeneration(id).stream()
                .collect(Collectors.toMap(v -> v.getVariable().getKey(), TemplateValue::getValue));
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            try (ZipOutputStream docx = new ZipOutputStream(output)) {
                ZipUtils.parse(
                        new ZipFile(new SeekableInMemoryByteChannel(generation.getTemplate().getData())),
                        docx,
                        (name, tmpl) -> {
                            try (Writer out = new StringWriter(); StringReader in = new StringReader(tmpl)) {
                                freemarker.template.Template t = new freemarker.template.Template(name, in, freemarkerConfig);
                                t.process(model, out);
                                return out.toString();
                            }
                        },
                        name -> imageService.findOne(Long.valueOf(model.get(name)))
                );
            }
            return save(generation.setData(output.toByteArray()).setContentType(generation.getTemplate().getContentType()));
        } catch (Throwable t) {
            throw new IllegalStateException(t);
        }
    }

    @Override
    public Long findAuthor(Long id) {
        return repository.findAuthor(id);
    }

    @Override
    public List<Generation> findByTemplate(Long templateId) {
        return repository.findAllByTemplate_Id(templateId);
    }
}