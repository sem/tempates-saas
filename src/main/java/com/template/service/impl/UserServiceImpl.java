package com.template.service.impl;

import com.template.model.User;
import com.template.repository.UserRepository;
import com.template.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Taras Zubrei
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends BaseService<User, Long> implements UserService {
    private final UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByUsername(username);
    }

    @Override
    protected JpaRepository<User, Long> repository() {
        return repository;
    }

    @Override
    public boolean existsByUsername(String username) {
        return repository.exists(Example.of(new User().setUsername(username)));
    }
}
