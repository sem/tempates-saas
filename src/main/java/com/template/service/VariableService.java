package com.template.service;

import com.template.model.Variable;

import java.util.List;

/**
 * @author Taras Zubrei
 */
public interface VariableService extends Service<Variable, Long> {
    List<Variable> findByTemplate(Long templateId);

    List<Variable> findByGeneration(Long generationId);

    Long findAuthor(Long id);

    List<Variable> save(List<Variable> values);
}