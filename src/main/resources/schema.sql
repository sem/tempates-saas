CREATE SEQUENCE IF NOT EXISTS "users_id_seq"
  START WITH 1;
CREATE TABLE IF NOT EXISTS "users" (
  "id"       BIGINT       NOT NULL,
  "username" VARCHAR(45) UNIQUE,
  "password" VARCHAR(255) NOT NULL,
  "enabled"  BOOLEAN DEFAULT TRUE,
  "role"     VARCHAR(45)  NOT NULL,
  PRIMARY KEY ("id")
);

CREATE SEQUENCE IF NOT EXISTS "templates_id_seq"
  START WITH 1;
CREATE TABLE IF NOT EXISTS "templates" (
  "id"              BIGINT      NOT NULL,
  "name"            VARCHAR(45) NOT NULL,
  "created_at"      TIMESTAMP   NOT NULL,
  "last_updated_at" TIMESTAMP   NOT NULL,
  "data"            BYTEA,
  "content_type"    VARCHAR(255),
  "author"          BIGINT      NOT NULL REFERENCES "users" ("id"),
  PRIMARY KEY ("id")
);

CREATE SEQUENCE IF NOT EXISTS "images_id_seq"
  START WITH 1;
CREATE TABLE IF NOT EXISTS "images" (
  "id"           BIGINT      NOT NULL,
  "name"         VARCHAR(45) NOT NULL,
  "created_at"   TIMESTAMP   NOT NULL,
  "data"         BYTEA,
  "content_type" VARCHAR(255),
  "author"       BIGINT      NOT NULL REFERENCES "users" ("id"),
  PRIMARY KEY ("id")
);

CREATE SEQUENCE IF NOT EXISTS "generations_id_seq"
  START WITH 1;
CREATE TABLE IF NOT EXISTS "generations" (
  "id"           BIGINT       NOT NULL,
  "name"         VARCHAR(100) NOT NULL,
  "created_at"   TIMESTAMP    NOT NULL,
  "data"         BYTEA,
  "content_type" VARCHAR(255),
  "template"     BIGINT       NOT NULL,
  "author"       BIGINT       NOT NULL REFERENCES "users" ("id"),
  PRIMARY KEY ("id")
);

CREATE SEQUENCE IF NOT EXISTS "variables_id_seq"
  START WITH 1;
CREATE TABLE IF NOT EXISTS "variables" (
  "id"              BIGINT       NOT NULL,
  "key"             VARCHAR(255) NOT NULL,
  "created_at"      TIMESTAMP    NOT NULL,
  "last_updated_at" TIMESTAMP    NOT NULL,
  "template"        BIGINT       NOT NULL REFERENCES "templates" ("id"),
  PRIMARY KEY ("id")
);

CREATE TABLE IF NOT EXISTS "template_values" (
  "generation_id" BIGINT NOT NULL REFERENCES "generations" ("id"),
  "variable_id"   BIGINT NOT NULL REFERENCES "variables" ("id"),
  "value"         TEXT   NOT NULL,
  PRIMARY KEY ("generation_id", "variable_id")
);
